import random
import functools
from collections.abc import Iterable
from math import prod


def factorize(n):
    ret = []
    candidate = 2
    while n > 1:
        if n % candidate == 0:
            ret.append(candidate)
            n = n // candidate
        else:
            candidate = candidate + 1
    return ret


def intersection_and_compliment(set1, set2):
    inter = []
    comp = []
    while len(set1) + len(set2) != 0:
        if len(set1) != 0 and len(set2) != 0:
            if set1[0] == set2[0]:
                inter.append(set1[0])
                set1 = set1[1:]
                set2 = set2[1:]
            elif set1[0] >= set2[0]:
                comp.append(set2[0])
                set2 = set2[1:]
            else:
                comp.append(set1[0])
                set1 = set1[1:]
        else:
            comp = comp + set1 + set2
            break
    return inter, comp


class Histogram:

    def __init__(self, init_value=None):
        self._sequences = dict()
        self._mod = 0
        if init_value is not None:
            if isinstance(init_value, ResultSequence):
                self._insert(init_value)
            elif isinstance(init_value, Iterable):
                for elem in init_value:
                    self._insert(elem)
            else:
                self._insert(init_value)

    def __add__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty():
                ret._import(other)
                return ret
            if other.is_empty():
                ret._import(self)
                return ret
            for l_val, l_occ in self._sequences.items():
                ret._mod = self._mod + other._mod
                for r_val, r_occ in other._sequences.items():
                    ret._insert(l_val + r_val, l_occ * r_occ)
        elif isinstance(other, int):
            ret._sequences = self._sequences
            ret._mod = self._mod + other
        return ret

    def __sub__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty():
                ret._import(other)
            elif other.is_empty():
                ret._import(self)
            else:
                ret._mod = self._mod + other._mod
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(l_val + r_val, l_occ * r_occ)
        elif isinstance(other, int):
            ret._sequences = self._sequences
            ret._mod = self._mod - other
        else:
            raise NotImplemented
        return ret

    def __mul__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                ret._mod = self._mod + other._mod
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(l_val * r_val, l_occ * r_occ)
        elif isinstance(other, int):
            ret._mod = self._mod * other
            ret._sequences = dict(map(lambda kv: (kv[0] * other, kv[1]), self._sequences.items()))
        else:
            raise NotImplemented
        return ret

    def __lt__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod < r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod < other)), occ)
        else:
            raise NotImplemented
        return ret

    def __gt__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod > r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod > other)), occ)
        else:
            raise NotImplemented
        return ret

    def __eq__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod == r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod == other)), occ)
        else:
            raise NotImplemented
        return ret

    def __ge__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod >= r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod >= other)), occ)
        else:
            raise NotImplemented
        return ret

    def __le__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod <= r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod <= other)), occ)
        else:
            raise NotImplemented
        return ret

    def __ne__(self, other):
        ret = Histogram()
        if isinstance(other, Histogram):
            if self.is_empty() or other.is_empty():
                return ret
            else:
                for l_val, l_occ in self._sequences.items():
                    for r_val, r_occ in other._sequences.items():
                        ret._insert(ResultSequence(int(l_val + self._mod != r_val + other._mod)), l_occ * r_occ)
        elif isinstance(other, int):
            for roll, occ in self._sequences.items():
                ret._insert(ResultSequence(int(roll + self._mod != other)), occ)
        else:
            raise NotImplemented
        return ret

    def _magnitude(self):
        ret = 0
        for _, occ in self._sequences.items():
            ret = ret + occ
        return ret

    def _import(self, other):
        self._sequences = other._sequences
        self._mod = other._mod

    def _insert(self, val, occ=1):
        if isinstance(val, ResultSequence):
            if val in self._sequences:
                self._sequences[val] = self._sequences[val] + occ
            else:
                self._sequences[val] = occ
        else:
            self._insert(ResultSequence(val), occ)

    def _total(self):
        return sum(self._sequences.values())

    def _scale(self, factor):
        for key, _ in self._sequences.items():
            self._sequences[key] = self._sequences[key] * factor

    def random(self):
        tot = self._total()
        select = random.randint(1, tot)
        for res, occ in self._sequences:
            select = select - occ
            if select < 1:
                return res

    def distribution(self):
        tot = self._total()
        ret = dict()
        for val, occ in self._sequences.items():
            roll = sum(val) + self._mod
            if roll in ret:
                ret[roll] = ret[roll] + float(occ) / float(tot)
            else:
                ret[roll] = float(occ)/float(tot)
        return dict(sorted(ret.items(), key=lambda x: x[0]))

    def average(self):
        tot = self._total()
        ret = 0.0
        for val, occ in self._sequences.items():
            ret = ret + (val.total() + self._mod) * occ/tot
        return ret

    def is_empty(self):
        return self._total() == 0

    def drop(self, top=0, bottom=0):
        new_entries = Histogram()
        for die, occ in self._sequences.items():
            new_entries._insert(die.drop(top, bottom), occ)
        self._sequences = new_entries._sequences

    def filter_sequences(self, *args):

        if len(args) > 1:
            for roll_filter in args:
                new_hist = self
                new_hist = new_hist.filter_sequences(roll_filter)
                return new_hist

        else:
            roll_filter = args[0]
            new_hist = Histogram()
            factors = []
            sub_histograms = []

            for die, occ in self._sequences.items():
                new_dice = roll_filter(die, self._mod)

                if isinstance(new_dice, Histogram):
                    magnitude_factors = factorize(new_dice._magnitude())
                    _, non_common = intersection_and_compliment(magnitude_factors, factors)
                    factors = sorted(factors + non_common)
                    new_hist._scale(prod(non_common + [1]))
                    sub_hist = Histogram()
                    for sub_die, sub_occ in new_dice._sequences.items():
                        sub_hist._insert(sub_die, occ * sub_occ)
                    sub_histograms.append((sub_hist, magnitude_factors))

                elif isinstance(new_dice, ResultSequence):
                    scale = prod(factors + [1])
                    new_hist._insert(new_dice, occ * scale)

                else:
                    raise NotImplemented

            for h, f in sub_histograms:
                _, missing_factors = intersection_and_compliment(factors, f)
                for new_die, new_occ in h._sequences.items():
                    new_hist._insert(new_die, new_occ * prod(missing_factors + [1]))

            return new_hist


@functools.total_ordering
class ResultSequence:

    def __init__(self, val=None):
        if val is not None:
            self.__results = [val]
            if val > ResultSequence.__highest_base:
                ResultSequence.__highest_base = val
        else:
            self.__results = []

    __highest_base = 0

    def __hash__(self):
        hash_base = ResultSequence.__highest_base
        res = 0
        for i in range(len(self.__results)):
            res = res + self.__results[i] * hash_base ** i
        return res

    def __add__(self, other):
        if isinstance(other, ResultSequence):
            ret = ResultSequence()
            ret.__results = self.__results + other.__results
            ret.__results = sorted(ret.__results)
        elif isinstance(other, int):
            ret = ResultSequence(sum(self.__results) + other)
        else:
            raise NotImplemented
        return ret

    def __mul__(self, other):
        if isinstance(other, ResultSequence):
            ret = ResultSequence(sum(self.__results) * sum(other.__results))
        elif isinstance(other, int):
            ret = ResultSequence()
            ret.__results = list(map(lambda x: x*other, self.__results))
        else:
            raise NotImplemented
        return ret

    def __len__(self):
        return len(self.__results)

    def __eq__(self, other):
        if isinstance(other, ResultSequence):
            if len(self) != len(other):
                return False
            else:
                for left, right in zip(self.__results, other.__results):
                    if left != right:
                        return False
                return True
        elif isinstance(other, int):
            return self.total() == other
        else:
            raise NotImplemented

    def __lt__(self, other):
        if isinstance(other, ResultSequence):
            return self.total() < other.total()
        elif isinstance(other, int):
            return self.total() < other
        else:
            raise NotImplemented

    def __gt__(self, other):
        if isinstance(other, ResultSequence):
            return self.total() > other.total()
        elif isinstance(other, int):
            return self.total() > other
        else:
            raise NotImplemented

    def __int__(self):
        return self.total()

    def __repr__(self):
        return repr(self.__results)

    def __iter__(self):
        for result in self.__results:
            yield result

    def drop(self, top=0, bottom=0):
        items = len(self.__results)
        ret = ResultSequence()
        ret.__results = self.__results[bottom: items - top]
        return ret

    def total(self):
        return sum(self.__results)

    def get(self, start, end=None):
        if end is not None:
            return self.__results[start:end]
        else:
            return self.__results[start]
